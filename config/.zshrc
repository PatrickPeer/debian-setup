# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# Set path if required
#export PATH=$GOPATH/bin:/usr/local/go/bin:$PATH

# Aliases
alias ls='ls --color=auto'
alias ll='ls -lah --color=auto'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
#alias ec="$EDITOR $HOME/.zshrc" # edit .zshrc
#alias sc="source $HOME/.zshrc"          # reload zsh configuration

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# Set up the prompt - if you load Theme with zplugin as in this example, this will be overiden by the Theme. If you comment out the Theme in zplugins, this will be loaded.
autoload -Uz promptinit
promptinit
prompt adam1            # see Zsh Prompt Theme below

# Use vi keybindings even if our EDITOR is set to vi
bindkey -v

bindkey '^R' history-incremental-search-backward

setopt histignorealldups sharehistory

# Keep 1000 lines of history within the shell and save it to ~/.zsh_history:
HISTSIZE=1000000
SAVEHIST=100000
HISTFILESIZE=500000
HISTFILE=~/.zsh_history

SSH_SESSION=0
if [ -n "$SSH_CLIENT" ] || [ -n "$SSH_TTY" ]; then
	SSH_SESSION=1
else
	case $(ps -o comm= -p $PPID) in
		sshd|*/sshd) SSH_SESSION=1;;
	esac
fi


# set tab title to cwd
precmd () {
	if [ "$SSH_SESSION" -eq "1" ]; then
		tab_label=$USER@$HOST:${PWD/${HOME}/\~} # use 'relative' path
	else
		tab_label=${PWD/${HOME}/\~} # use 'relative' path
	fi
	echo -ne "\e]2;${tab_label}\a" # set window title to full string
	echo -ne "\e]1;${tab_label: -24}\a" # set tab title to rightmost 24 characters
}

# Use modern completion system
autoload -Uz compinit
compinit

# zplug - manage plugins
source /usr/share/zplug/init.zsh
zplug "plugins/git", from:oh-my-zsh
zplug "plugins/sudo", from:oh-my-zsh
zplug "plugins/command-not-found", from:oh-my-zsh
zplug "zsh-users/zsh-syntax-highlighting"
zplug "zsh-users/zsh-autosuggestions"
zplug "zsh-users/zsh-history-substring-search"
zplug "zsh-users/zsh-completions"
zplug "junegunn/fzf"
zplug "romkatv/powerlevel10k", as:theme, depth:1
#zplug "themes/robbyrussell", from:oh-my-zsh, as:theme   # Theme

# zplug - install/load new plugins when zsh is started or reloaded
if ! zplug check --verbose; then
    printf "Install? [y/N]: "
    if read -q; then
        echo; zplug install
    fi
fi
zplug load #--verbose

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
if zmodload zsh/terminfo && (( terminfo[colors] >= 256 )); then
    # capable terminal
    [[ ! -f ~/.p10k.fancy.zsh ]] || source ~/.p10k.fancy.zsh
else
    # might be TTY or some other not very capable terminal
    [[ ! -f ~/.p10k.basic.zsh ]] || source ~/.p10k.basic.zsh
fi
