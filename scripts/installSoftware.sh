#!/bin/bash
script_directory="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
general_software="vim htop bpytop btop gedit flatpak"
desktop_software="spotify-client chromium terminology bless tilda gparted"
gnome_extensions="gnome-shell-extension-bluetooth-quick-connect gnome-shell-extension-system-monitor"

# install actual software
echo "Installing $general_software";
sudo nala install -y  $general_software

echo "Installing $desktop_software";
sudo nala install -y  $desktop_software

echo "Installing $gnome_extensions";
sudo nala install -y  $gnome_extensions
