#!/bin/bash

script_directory="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
fonts_directory=~/.local/share/fonts/
zsh_packages="zsh zplug fonts-powerline"

# install packages
sudo nala install -y $zsh_packages

# install font
mkdir -p $fonts_directory
cp $script_directory/../fonts/* $fonts_directory

# setup zsh
cp $script_directory/../config/.zshrc ~/
cp $script_directory/../config/.p10k.basic.zsh ~/
cp $script_directory/../config/.p10k.fancy.zsh ~/
chsh -s `which zsh` `whoami`

# start and install plugins
zsh
