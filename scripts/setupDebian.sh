#!/bin/bash

sources_list=/etc/apt/sources.list

sudo sed -i -e'/#/d' $sources_list
sudo sed -i -e'/deb cdrom/d' $sources_list
sudo apt-get install -y git

git clone https://gitlab.com/PatrickPeer/debian-setup.git

echo "Setting up nala"
debian-setup/scripts/setupNala.sh

echo "Installing software"
debian-setup/scripts/installSoftware.sh

echo "Setting configuration valueks"
debian-setup/scripts/setConfigValues.sh

echo "Setting up zsh"
debian-setup/scripts/setupZsh.sh
