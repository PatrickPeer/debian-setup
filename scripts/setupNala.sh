#!/bin/bash

sources_list=/etc/apt/sources.list

# edit sources.list
sudo cp -a $sources_list $sources_list.bak
sudo sed -i -e'/#/d' $sources_list
sudo sed -i -e'/deb cdrom/d' $sources_list
sudo sed -i -e's/main/main contrib non-free/g' $sources_list

echo "" | sudo tee -a $sources_list
echo "# Unstable and experimental" | sudo tee -a $sources_list
echo "#deb http://ftp.at.debian.org/debian/ sid main contrib non-free" | sudo tee -a $sources_list
echo "#deb http://ftp.at.debian.org/debian/ experimental main contrib non-free" | sudo tee -a $sources_list

# setup third party repositories
echo "" | sudo tee -a $sources_list
echo "# third party repos" | sudo tee -a $sources_list
echo "# # curl -sS https://download.spotify.com/debian/pubkey_7A3A762FAFD4A51F.gpg | sudo gpg --dearmor --yes -o /etc/apt/trusted.gpg.d/spotify.gpg" | sudo tee -a $sources_list
echo "deb http://repository.spotify.com stable non-free" | sudo tee -a $sources_list

# install keys
sudo apt install -y curl
sudo curl -sS https://download.spotify.com/debian/pubkey_7A3A762FAFD4A51F.gpg | sudo gpg --dearmor --yes -o /etc/apt/trusted.gpg.d/spotify.gpg

# install and prepare nala
sudo apt install -y nala
sudo nala fetch
sudo nala upgrade
