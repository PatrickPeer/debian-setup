# Debian Setup

Scripts to set up Debian after a fresh install, to how I like it.

Just execute
`wget -O - https://gitlab.com/PatrickPeer/debian-setup/-/raw/master/scripts/setupDebian.sh | bash`
